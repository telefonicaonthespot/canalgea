<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Carbon\Carbon;

class IndexController extends Controller
{
    public function index(){
        $fec_consulta=Carbon::now();
        if (config('app.debug'))
            $fec_consulta=Carbon::parse('2023-10-28');

        $sorteos_loto=DB::table('stl_ventas_sorteos')
            ->join('stl_sorteos','stl_ventas_sorteos.cod_sorteo','stl_sorteos.cod_sorteo')
            ->where('venta_total_sorteo','<>',0)
            ->where('id_sorteo','<>',0)
            ->wherein('stl_sorteos.cod_sorteo',explode(",",\Config::get('app.sorteos_loto')))
            ->orderby('stl_sorteos.DES_SORTEO')
            ->get();

        $sorteos_resto=DB::table('stl_ventas_sorteos')
            ->join('stl_sorteos','stl_ventas_sorteos.cod_sorteo','stl_sorteos.cod_sorteo')
            ->where('venta_total_sorteo','<>',0)
            ->where('id_sorteo','<>',0)
            ->wherein('stl_sorteos.cod_sorteo',explode(",",\Config::get('app.sorteos_resto')))
            ->orderby('stl_sorteos.DES_SORTEO')
            ->get();

        $graficos_quesito=DB::table('stl_ventas_sorteos')
            ->join('stl_sorteos','stl_ventas_sorteos.cod_sorteo','stl_sorteos.cod_sorteo')
            ->where('venta_total_sorteo','<>',0)
            ->where('id_sorteo','<>',0)
            ->wherenotin('stl_sorteos.cod_sorteo',["J11","J12"])
            ->orderby('stl_sorteos.DES_SORTEO')
            ->get();


        $datos_genericos=DB::table('stl_datosgenericos')
            ->orderby('fec_insercion','desc')
            ->first();

        $lot_nacional=DB::table('stl_loterianacional')
            ->join('stl_sorteos','stl_loterianacional.cod_sorteo','stl_sorteos.cod_sorteo')
            ->where('stl_loterianacional.BILLETES_SORTEO', '<>', 0)
            ->whereraw('month(stl_loterianacional.fec_sorteo_ln)<>12')
            ->whereraw('day(stl_loterianacional.fec_sorteo_ln)<>22')
            ->orderby('stl_loterianacional.fec_sorteo_ln')
            ->get();

        $lot_navidad=DB::table('stl_loterianacional')
            ->join('stl_sorteos','stl_loterianacional.cod_sorteo','stl_sorteos.cod_sorteo')
            ->where('stl_loterianacional.BILLETES_SORTEO', '<>', 0)
            ->whereraw('month(stl_loterianacional.fec_sorteo_ln)=12')
            ->whereraw('day(stl_loterianacional.fec_sorteo_ln)=22')
            ->distinct()
            ->orderby('stl_loterianacional.fec_sorteo_ln')
            ->first();
;

        $lot_ninio=DB::table('stl_loterianacional')
            ->join('stl_sorteos','stl_loterianacional.cod_sorteo','stl_sorteos.cod_sorteo')
            ->where('stl_loterianacional.BILLETES_SORTEO', '<>', 0)
            ->whereraw('month(stl_loterianacional.fec_sorteo_ln)=1')
            ->whereraw('day(stl_loterianacional.fec_sorteo_ln)=5')
            ->distinct()
            ->orderby('stl_loterianacional.fec_sorteo_ln')
            ->first();

        $datos_financieros=DB::table('stl_datos_financieros')->get();
        
        return view('index',compact('sorteos_loto','sorteos_resto','graficos_quesito','datos_financieros','datos_genericos','lot_nacional','lot_navidad','lot_ninio'));
    }

    public function cabecera_terminales(){
        $datos_genericos=DB::table('stl_datosgenericos')
            ->orderby('fec_insercion','desc')
            ->first();
        return view('cabecera_terminales',compact('datos_genericos'));    
    }

    public function venta_semanal($juegos){
        $fec_consulta=Carbon::now();
        if (config('app.debug'))
			$fec_consulta=Carbon::parse('2023-10-28');
        $venta_semanal=DB::table('stl_datosorteos')
            ->join('stl_sorteos','stl_datosorteos.cod_juego','stl_sorteos.cod_sorteo')
            ->where('fec_ins',$fec_consulta->toDateString())
            ->where('hora_ins','>',400)
            ->orderby('venta_total')
            ->orderby('hora_ins')
            ->get();

        $datos=$venta_semanal->wherein('cod_juego',explode(",",$juegos));

        return view('grafico_venta_semanal',compact('datos'));
    }

    public function por_hora($tipo) {
        $fec_consulta=Carbon::now();
        if (\Config::get('APP_DEBUG'))
            $fec_consulta=Carbon::parse('2022-02-15');
        $leyendas=["Hoy","Semana pasada"];
        $colores=["#FAD76B","#4B83B2"];
       

        $datos=DB::table('stl_finanzas_xhora')
            ->where('fec_ins',$fec_consulta->toDateString())
            ->get();
        

        $laotra_fecha=$fec_consulta->subDays(7);
        $datos_ant=DB::table('stl_finanzas_xhora')
            ->where('fec_ins',$laotra_fecha->toDateString())
            ->get();

        //Aver que campo hay que leer
        switch ($tipo) {
            case 'ventas':
                $campo="VENT_TOT";
                $etiqueta="Ventas";
                $titulo="Ventas acumuladas por hora [".Carbon::now()->format('d/m/Y').']';
                break;
            case 'terminales':
                $campo="term_conect";
                $etiqueta="Terminales";
                $titulo="Terminales conectados por hora [".Carbon::now()->format('d/m/Y').']';
                break;
            case 'transacciones':
                $campo="TRANS_TOT";
                $etiqueta="Transacciones";
                $titulo="Transacciones acumuladas por hora [".Carbon::now()->format('d/m/Y').']';
                break;
        }

        
        return view('grafico_porhora',compact('datos','datos_ant','campo','etiqueta','titulo','leyendas','colores'));
        
    }

    public function vsanio($tipo) {
        $fec_consulta=Carbon::now();
        
        if (config('app.debug'))
            $fec_consulta=Carbon::parse('2023-10-28');
        $leyendas=["Hoy","Año pasado"];
        $colores=["#FAD76B","#26AC00"];

        $datos=DB::table('stl_finanzas_xhora')
            ->where('fec_ins',$fec_consulta->toDateString())
            ->get();
        
        $laotra_fecha=carbon::parse(getStartAndEndDate($fec_consulta->weekOfYear,$fec_consulta->year-1,$fec_consulta->dayOfWeek));
  
        $datos_ant=DB::table('stl_finanzas_xhora')
            ->where('fec_ins',$laotra_fecha->toDateString())
            ->get();

        //Aver que campo hay que leer
        switch ($tipo) {
            case 'ventas':
                $campo="VENT_TOT";
                $etiqueta="Ventas";
                $titulo="Ventas acumuladas por hora [".Carbon::now()->format('d/m/Y').'] vs '.dayOfWeek2($laotra_fecha->dayOfWeek,2,"I").' semana '.$laotra_fecha->weekOfYear.' '.$laotra_fecha->format('Y');
                break;
            case 'terminales':
                $campo="term_conect";
                $etiqueta="Terminales";
                $titulo="Terminales conectados por hora [".Carbon::now()->format('d/m/Y').'] vs '.dayOfWeek2($laotra_fecha->dayOfWeek,2,"I").' semana '.$laotra_fecha->weekOfYear.' '.$laotra_fecha->format('Y');
                break;
            case 'transacciones':
                $campo="TRANS_TOT";
                $etiqueta="Transacciones";
                $titulo="Transacciones acumuladas por hora [".Carbon::now()->format('d/m/Y').'] vs '.dayOfWeek2($laotra_fecha->dayOfWeek,2,"I").' semana '.$laotra_fecha->weekOfYear.' '.$laotra_fecha->format('Y');
                break;
        }

        
        return view('grafico_porhora',compact('datos','datos_ant','campo','etiqueta','titulo','leyendas','colores'));
        
    }

    public function ventas_acumuladas($juegos){
        $fec_consulta=Carbon::now();
        if (config('app.debug'))
			$fec_consulta=Carbon::parse('2023-10-28');

        
        $venta_semanal=DB::table('stl_datosorteos')
            ->select('COD_SORTEO','DES_SORTEO','val_color')
            ->selectraw('max(venta_total) as venta_total')
            ->join('stl_sorteos','stl_datosorteos.cod_juego','stl_sorteos.cod_sorteo')
            ->where('fec_ins',$fec_consulta->toDateString())
            ->groupBy('cod_sorteo')
            ->groupBy('des_sorteo')
            ->groupBy('val_color')
            ->orderby('cod_juego')
            ->get();


        $datos=$venta_semanal->wherein('COD_SORTEO',explode(",",$juegos));

        $laotra_fecha=$fec_consulta->subDays(7);
        $venta_ant=DB::table('stl_sorteos')
            ->select('COD_SORTEO','DES_SORTEO','val_color')
            ->selectraw('ifnull(max(venta_total),0) as venta_total')
            ->leftjoin('stl_datosorteos','stl_datosorteos.cod_juego','stl_sorteos.cod_sorteo')
            ->where('fec_ins',$laotra_fecha->toDateString())
            ->groupBy('cod_sorteo')
            ->groupBy('des_sorteo')
            ->groupBy('val_color')
            ->orderby('cod_juego')
            ->get();


        $datos_ant=$venta_ant->wherein('COD_SORTEO',explode(",",$juegos));
        return view('grafico_venta_acumulada',compact('datos','datos_ant'));
    }
}
