<?php
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use App\Classes\RandomColor;


function isAdmin(){
    if(Auth::user()){
        return Auth::User()->val_nivel_acceso == 200 ? true : false;
    } else return false;
    
}

function randomcolor(){
    return RandomColor::one();
}

function lz($num)
{
    return (strlen($num) < 2) ? "0{$num}" : $num;
}

function decimal_to_time($dec,$default=0,$thru=0)
{
    try{
        $dec<0?$negativo="-": $negativo="";
        $dec = abs($dec);
        // start by converting to seconds
        $seconds = ($dec * 3600);
        // we're given hours, so let's get those the easy way
        $hours = floor($dec);
        // since we've "calculated" hours, let's remove them from the seconds variable
        $seconds -= $hours * 3600;
        // calculate minutes left
        $minutes = floor($seconds / 60);
        // remove those from seconds as well
        $seconds -= $minutes * 60;
        // return the time formatted HH:MM:SS
        if ($seconds>30){
            $minutes++;
        }
        if ($minutes==60){
            $minutes=0;
            $hours++;
        }
        if($thru!=0){
            return round($dec,2);
        }
        if ($hours==0 && $minutes==0){
            return $default;
        } else {
            return $negativo.lz($hours).":".lz($minutes);
        }
    } catch(\Exception $e){
        return $dec;
    }
   
}


function convertHM($time) {
    return date('H:i', mktime(0,$time));
}

function decToHours($h)
{
    if (!$h) {
        return "00:00";
    }
    $_h = explode(".",$h);
    if (isset($_h[1])) {
        $m = substr(($_h[1]*60)/100,0,2);
        if ($m<10) {
            $m = "0".$m;
        }
    }else{
        $m = "00";
    }
    return ($_h[0] <= 9 ? "0".$_h[0] : $_h[0]).':'.($m);
}

function adaptar_fecha($d){
    try{    
        if (Carbon::createFromFormat('d/m/Y H:i:s', $d)!== false) {
            return Carbon::createFromFormat('d/m/Y H:i:s', $d)->format('Y-m-d H:i:s');
        }
    } catch (\Exception $e){}
    try{
        if (Carbon::createFromFormat('d/m/Y H:i', $d)!== false) {
            return Carbon::createFromFormat('d/m/Y H:i', $d)->format('Y-m-d H:i');
        }   
    } catch (\Exception $e){}    
    try{
        if (Carbon::createFromFormat('d/m/Y', $d)!== false) {
            return Carbon::createFromFormat('d/m/Y', $d)->format('Y-m-d');
        }
    } catch (\Exception $e){}
    try{    
        if (Carbon::createFromFormat('Y-m-d', $d)!== false) {
            return Carbon::createFromFormat('d/m/Y', $d)->format('Y-m-d');
        }
    } catch (\Exception $e){}
    try{
        if (Carbon::parse($d)!== false) {
            return Carbon::parse($d)->format('Y-m-d');
        }
    } catch (\Exception $e){}
    return  $d;
}

function beauty_fecha($date,$mostrar_hora=-1){
    setlocale(LC_TIME, App::getLocale());
    $hora="";
    if((strlen($date)>10 && $mostrar_hora==-1) || $mostrar_hora==1){
        $hora=Carbon::parse($date)->format('H:i');
    }
    if(Carbon::parse($date)->format('Y')==Carbon::now()->format('Y')){
        $fecha=Carbon::parse($date)->formatLocalized('%d %b');
    } else {
        $fecha=Carbon::parse($date)->format('d/m/Y');
    }
    return $fecha.' <span style="font-size: 0.8em">'.$hora.'</span>';
}

function random_readable_pwd($length=10){
 
    // the wordlist from which the password gets generated 
    // (change them as you like)
    $words = 'dog,cat,sheep,sun,sky,red,ball,happy,ice,';
    $words .= 'green,blue,music,movies,radio,green,turbo,';
    $words .= 'mouse,computer,paper,water,fire,storm,chicken,';
    $words .= 'boot,freedom,white,nice,player,small,eyes,';
    $words .= 'path,kid,box,black,flower,ping,pong,smile,';
    $words .= 'coffee,colors,rainbow,plus,king,tv,ring';
 
    // Split by ",":
    $words = explode(',', $words);
    if (count($words) == 0){ die('Wordlist is empty!'); }
 
    // Add words while password is smaller than the given length
    $pwd = '';
    while (strlen($pwd) < $length){
        $r = mt_rand(0, count($words)-1);
        $pwd .= $words[$r];
    }
 
    // append a number at the end if length > 2 and
    // reduce the password size to $length
    $num = mt_rand(1, 99);
    if ($length > 2){
        $pwd = substr($pwd,0,$length-strlen($num)).$num;
    } else { 
        $pwd = substr($pwd, 0, $length);
    }
 
    return $pwd;
 
}

function tz_list() {
    $zones_array = array();
    $timestamp = time();
    $dummy_datetime_object = new DateTime();
    foreach(timezone_identifiers_list() as $key => $zone) {
        date_default_timezone_set($zone);
        $zones_array[$key]['zone'] = $zone;
        $zones_array[$key]['diff_from_GMT'] = 'GMT' . date('P', $timestamp);

        $tz = new DateTimeZone($zone);
        $zones_array[$key]['offset'] = $tz->getOffset($dummy_datetime_object);
    }

    return $zones_array;
}

function regions(){
    $regions = array(
        'Africa' => DateTimeZone::AFRICA,
        'America' => DateTimeZone::AMERICA,
        'Antarctica' => DateTimeZone::ANTARCTICA,
        'Asia' => DateTimeZone::ASIA,
        'Atlantic' => DateTimeZone::ATLANTIC,
        'Europe' => DateTimeZone::EUROPE,
        'Indian' => DateTimeZone::INDIAN,
        'Pacific' => DateTimeZone::PACIFIC
    );

    return $regions;
}

function icono_nombre($nombre,$height=50,$font=18){
    
    try{
        $padding=intdiv($height,11);
        $rand=Str::random(9);
        $words = explode(" ", $nombre);
        $acronym = "";
        $i = 0;
        foreach ($words as $w) {
            $acronym .= $w[0];
            if (++$i == 2) break;
        }
    } catch(\Exception $e){
        $acronym=substr($nombre,1,2);
    }
    //return '<span class="round" id="'.$rand.'" style="text-transform: uppercase; background-color: '.App\Classes\RandomColor::one().'">'.$acronym.'</span>';
    return '<span class="round tooltipster" id="'.$rand.'" style="font-weight: 500; font-size: '.$font.'px; width: '.$height.'px;height: '.$height.'px; padding-top:'.$padding.'px; text-transform: uppercase; background-color: '.genColorCodeFromText($nombre).'" data-toggle="tooltip" data-placement="bottom" title="'.$nombre.'">'.$acronym.'</span>';
}

function mensaje_excepcion($e){
    if(fullAccess()){
        return $e->getMessage().' {'.get_class($e).'}  ['.debug_backtrace()[1]['function'].']';
    } else {
        return substr($e->getMessage(),1,15);
    }
}

function request_tabla($tabla)
{
    $data=DB::table($tabla)->first();
    return response()->json([
        "response" => "ok",
        "request"=> $data,
        "TS" => Carbon::now()->format('Y-m-d h:i:s')
    ]); 
}

function fields_tabla($tabla)
{
    return response()->json([
        "response" => "ok",
        "fields"=> DB::select('describe '.$tabla),
        "TS" => Carbon::now()->format('Y-m-d h:i:s')
    ]); 
}

function valor_total($coleccion,$columna,$formato='time',$thru=0,$nozero=0){
    try{
            $suma=$coleccion->sum($columna);
            if($formato=='time'){
                $result=decimal_to_time($suma,0,$thru);
                if(($result==0 && $nozero!=0)||$result!=0){
                    return decimal_to_time($suma,0,$thru);
                } else {
                    return "";
                }
            } else if($formato='num'){
                return $suma;
            }
            if ($nozero!=0){
                return "";
            } else {
                return 0;
            }
        } catch(\Exception $e){
            return $e;
        }
}

function genColorCodeFromText($text,$min_brightness=100,$spec=10)
{
	// Check inputs
	if(!is_int($min_brightness)) throw new Exception("$min_brightness is not an integer");
	if(!is_int($spec)) throw new Exception("$spec is not an integer");
	if($spec < 2 or $spec > 10) throw new Exception("$spec is out of range");
	if($min_brightness < 0 or $min_brightness > 255) throw new Exception("$min_brightness is out of range");
	
	
	$hash = md5($text);  //Gen hash of text
	$colors = array();
	for($i=0;$i<3;$i++)
		$colors[$i] = max(array(round(((hexdec(substr($hash,$spec*$i,$spec)))/hexdec(str_pad('',$spec,'F')))*255),$min_brightness)); //convert hash into 3 decimal values between 0 and 255
		
	if($min_brightness > 0)  //only check brightness requirements if min_brightness is about 100
		while( array_sum($colors)/3 < $min_brightness )  //loop until brightness is above or equal to min_brightness
			for($i=0;$i<3;$i++)
				$colors[$i] += 10;	//increase each color by 10
				
	$output = '';
	
	for($i=0;$i<3;$i++)
		$output .= str_pad(dechex($colors[$i]),2,0,STR_PAD_LEFT);  //convert each color to hex and append to output
	
	return '#'.$output;
}

function gitVersion() 
{
    exec('git describe --always',$version_mini_hash);
    exec('git rev-list HEAD | wc -l',$version_number);
    exec('git log -1',$line);
    $version['short'] = "v1.".trim($version_number[0]);//.".".$version_mini_hash[0];
    $version['full'] = "v1.".trim($version_number[0]).".$version_mini_hash[0] (".str_replace('commit ','',$line[0]).")";
    return $version;
}

function decodeComplexJson($string) { # list from www.json.org: (\b backspace, \f formfeed)
    $string = preg_replace("/[\r\n]+/", "", $string); //Retornos de carro
    $string = preg_replace('/[ ]{2,}|[\t]/', '', trim($string));  //tabs
    $json = utf8_encode($string);
    $json = json_decode($json);
    return $json;
}

function dayOfWeek2($num,$len=1,$tipo="E"){
    try{
        if($tipo=="E"){
            if($len==1){
                $days = array(
                    1 => 'Lu',
                    2 => 'Ma',
                    3 => 'Mi',
                    4 => 'Ju',
                    5 => 'Vi',
                    6 => 'Sa',
                    7 => 'Do'
                );
            }
            
            if($len==2){
                $days = array(
                    1 => 'Lunes',
                    2 => 'Martes',
                    3 => 'Miércoles',
                    4 => 'Jueves',
                    5 => 'Viernes',
                    6 => 'Sábado',
                    7 => 'Domingo'
                );
            }
        }
        if($tipo=="I"){
            if($len==1){
                $days = array(
                    2 => 'Lu',
                    3 => 'Ma',
                    4 => 'Mi',
                    5 => 'Ju',
                    6 => 'Vi',
                    0 => 'Sa',
                    1 => 'Do'
                );
            }
            
            if($len==2){
                $days = array(
                    2 => 'Lunes',
                    3 => 'Martes',
                    4 => 'Miércoles',
                    5 => 'Jueves',
                    6 => 'Viernes',
                    0 => 'Sábado',
                    1 => 'Domingo'
                );
            }
        }

        return $days[$num];
    } catch(\Exception $e){
        return "error DoW ".$num;
    }
}

function txt_blanco($hex) {
    // returns brightness value from 0 to 255
    // strip off any leading #
    $hex = str_replace('#', '', $hex);
   
    $c_r = hexdec(substr($hex, 0, 2));
    $c_g = hexdec(substr($hex, 2, 2));
    $c_b = hexdec(substr($hex, 4, 2));
   
    $result=(($c_r * 299) + ($c_g * 587) + ($c_b * 114)) / 1000;
    if($result<130){
        return "text-white";
    }
   }

function valor($parametros,$nombre){
    $salida=null;
    try{
        foreach($parametros as $param){
            if($param->name==$nombre){
                return $param->value;
            }
        }
    } catch(\Exception $e){
    }
    return $salida;
}

function getStartAndEndDate($week, $year,$day) {
    $dateTime = new DateTime();
    $dateTime->setISODate($year, $week);
    $result=$dateTime;
    $dateTime->modify('+'.$day.' days');
    
    return $result;
  }