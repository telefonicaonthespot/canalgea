<div class="row" style="font-size: 40px">
    <div class="col-md-4 text-center" style="color: #000">
       <span style="font-size: 26px">TERMINALES CONECTADOS</span><br> @isset($datos_genericos) <b>{{ number_format($datos_genericos->TERM,0,",",".") }}@endisset</b>
    </div>
    <div class="col-md-4 text-center" style="color: #000">
        <span style="font-size: 26px"> TRANSACCIONES DEL ULTIMO MINUTO</span><br>@isset($datos_genericos) <b>{{ number_format($datos_genericos->TRAN,0,",",".") }}@endisset</b>
    </div>
    <div class="col-md-4 text-center" style="color: #000">
        <span style="font-size: 26px"> VENTAS TOTALES EN ESTA SEMANA</span><br>@isset($datos_genericos) <b>{{ number_format($datos_genericos->VENT,0,",",".") }} €@endisset</b>
    </div>
</div>