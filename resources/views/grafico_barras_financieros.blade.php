
<div class="card">
    <div class="card-body">
        <h4>Datos financieros de la semana</h4>
        <div id="bar-chart" style="width:410px; height:270px;" class="grafico"></div>
    </div>
</div>

@section('scripts3')
<script>
// ============================================================== 
// Bar chart option
// ============================================================== 

var myChart = echarts.init(document.getElementById('bar-chart'));

// specify chart configuration item and data
option = {
    tooltip : {
        trigger: 'axis'
    },
    grid: {
        left: 60,
        top: 20,
        right: 10,
        bottom: 50
    },
    legend: {
        data:['Ventas activos','Pagos activos','Pagos Lotería Nacional'],
        show: true,
        width: '100%',
        bottom: 0,
        left: 0,
        height: 20,
        type: 'plain',
        orient: 'horizontal',
    },
    toolbox: {
        show : false,
        feature : {
            
            magicType : {show: true, type: ['line', 'bar']},
            restore : {show: false},
            saveAsImage : {show: true}
        }
    },

    color: ["#96bcf2","#5fef17","#ffb200"],
    calculable : true,
    xAxis : [
        {
            type : 'category',
             data : [@foreach($datos_financieros as $fi) "{{ dayOfWeek2(substr($fi->COD_DIA,2,1)) }}", @endforeach]
        }
    ],
    yAxis : [
        {
            type : 'value',
            axisLabel: {
                formatter: '{value}K', 
            }
        }
    ],
    series : [
            {
                name:'Ventas activos',
                type:'bar',
                data:[ @foreach($datos_financieros as $f)"{{round($f->VENTAS_ACTIVOS/1000)}}",@endforeach],
            },
            {
                name:'Pagos activos',
                type:'bar',
                data:[ @foreach($datos_financieros as $f)"{{round($f->PAGOS_ACTIVOS/1000)}}",@endforeach],
            },
            {
                name:'Pagos Lotería Nacional',
                type:'bar',
                data:[ @foreach($datos_financieros as $f)"{{round($f->PAGOS_LOTNAC/1000)}}",@endforeach],
            },
        ]
};
                    

// use configuration item and data specified to show chart
myChart.setOption(option, true), $(function() {
            function resize() {
                setTimeout(function() {
                    myChart.resize()
                }, 100)
            }
            $(window).on("resize", resize), $(".sidebartoggler").on("click", resize)
        });

</script>
@endsection