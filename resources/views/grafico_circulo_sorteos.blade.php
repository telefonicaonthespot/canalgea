@php


$colores=array();

foreach($graficos_quesito as $g){
    //dd($g);
    $colores[]=$g->val_color;
    
}
$n=0;

header("Cache-Control: no-cache, no-store, must-revalidate"); // HTTP 1.1.
    header("Pragma: no-cache"); // HTTP 1.0.
    header("Expires: 0"); // Proxies.

    
@endphp
<div>
    <h4>Porcentaje de ventas semanal por juego</h4>
</div>
<div id="sales-donute" style="width: 100%; height: 350px;" class="grafico">
    
</div>


@section('scripts2')
<script>

$(function(){
    // ============================================================== 
    // doughnut chart option
    // ============================================================== 
    var doughnutChart = echarts.init(document.getElementById('sales-donute'));
    // specify chart configuration item and data
    option = {
        tooltip: {
            trigger: 'item',
            triggerOn: 'click',
            formatter: "{b}: {c} ({d}%)",
            position: ['50%', '50%']
        },
         legend: {
             show: true,
             width: '100%',
             bottom: 0,
             left: 0,
             height: 70,
             type: 'plain',
             orient: 'vertical',
             data: [@foreach($graficos_quesito as $g)'{{$g->DES_SORTEO}}',@endforeach]
        }
        , color: {!!json_encode($colores)!!}
        , calculable: false
        , series: [
            {
                name: 'Source',
                type: 'pie',
                radius: ['35%', '60%'],
                startAngle: 75,
                label: {
                    normal: {
                        show: true,
                        formatter: '{b} {d}%'
                    },
                    emphasis: {
                        show: true,
                        textStyle: {
                            fontSize: '22',
                            fontWeight: 'bold'
                        }
                    }
                },
                labelLine: {
                    normal: {
                        show: true
                    }
                },
                data: [
                    @foreach($graficos_quesito as $g){value: {{round($g->VENTA_TOTAL_SORTEO)}}, name:'{{$g->abreviatura}}'},@endforeach    
            ]
        }
    ]
    }
    
    // use configuration item and data specified to show chart
    doughnutChart.setOption(option, true), $(function () {
        function resize() {
            setTimeout(function () {
                doughnutChart.resize()
            }, 100)
        }
        $(window).on("resize", resize), $(".sidebartoggler").on("click", resize)
    });

});

</script>
@endsection