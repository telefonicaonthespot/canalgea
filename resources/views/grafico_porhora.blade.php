{{-- <script src="/vendor/echarts/dist/echarts.min.js"></script> --}}
@php
    //$horas=$datos_ant->pluck('HORA_INS')->unique();
    $rnd=Illuminate\Support\Str::random(4);   

    $horas=[];
    for($n=0;$n<=23;$n++){
        $horas[]=str_pad($n, 2, '0', STR_PAD_LEFT).':00';
    }

    header("Cache-Control: no-cache, no-store, must-revalidate"); // HTTP 1.1.
    header("Pragma: no-cache"); // HTTP 1.0.
    header("Expires: 0"); // Proxies.

    
@endphp

<div class="card">
    <h5 class="mt-1 text-center col-12 mb-2" >{{$titulo}}</h5>
    <div class="card-body pt-0 pb-0">
        <div id="bar-chart{{$rnd}}" style="width:1360px; height:180px;" class="grafico"></div>
    </div>
</div>


<script>
// ============================================================== 
// Bar chart option
// ============================================================== 

var myChart{{$rnd}} = echarts.init(document.getElementById('bar-chart{{$rnd}}'));

// specify chart configuration item and data
option{{$rnd}} = {
    tooltip : {
        trigger: 'axis'
    },
    grid: {
        left: 80,
        top: 20,
        right: 40,
        bottom: 20
    },
    legend: {
        data:{!! json_encode($leyendas) !!},
        show: true,
        width: '100%',
        bottom: 0,
        top: 0,
        left: '8%',
        height: 20,
        type: 'plain',
        orient: 'horizontal',
    },
    toolbox: {
        show : false,
        feature : {
            magicType : {show: true, type: ['line', 'bar']},
            restore : {show: false},
            saveAsImage : {show: true}
        }
    },

    color: {!! json_encode($colores) !!},
    calculable : true,
    xAxis : [
        {
            type : 'category',
            data : [@foreach($horas as $h) "{{ substr(str_pad($h, 4, '0', STR_PAD_LEFT),0,2) }}", @endforeach]
        }
    ],
    yAxis : [
        {
            type : 'value',
            axisLabel: {
                formatter: '{value}', 
            }
        }
    ],
    series : [
            {
                name:'{{$leyendas[1]}}',
                type:'line',
                showAllSymbol: true,
                data:[ @foreach($datos_ant as $d)"{{round($d->$campo)}}",@endforeach],
                //areaStyle: {}
                lineStyle: {width: 6}
            },
            {
                name:'{{$leyendas[0]}}',
                type:'line',
                showAllSymbol: true,
                data:[ @foreach($datos as $d)"{{round($d->$campo)}}",@endforeach],
                //areaStyle: {}
                lineStyle: {width: 6}
            }
        ]
};
// use configuration item and data specified to show chart
myChart{{$rnd}}.setOption(option{{$rnd}}, true), $(function() {
            function resize() {
                setTimeout(function() {
                    myChart{{$rnd}}.resize()
                }, 50)
            }
            //$(window).on("resize", resize), $(".sidebartoggler").on("click", resize)
        });

</script>