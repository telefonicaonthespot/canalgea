

  {{-- <script src="/vendor/echarts/dist/echarts.min.js"></script> --}}

@php
    $juegos=$datos->pluck('COD_SORTEO')->unique();
    $nomjuegos=$datos->pluck('DES_SORTEO')->unique();
    $colores=$datos->pluck('val_color')->unique();

    $rnd=Illuminate\Support\Str::random(4);   

    header("Cache-Control: no-cache, no-store, must-revalidate"); // HTTP 1.1.
    header("Pragma: no-cache"); // HTTP 1.0.
    header("Expires: 0"); // Proxies.
    
@endphp

<div class="card">
    <div class="card-body">
    <div id="bar-chart{{$rnd}}" style="width:1360px; height:290px;" class="grafico"></div>
    </div>
</div>



<script>
// ============================================================== 
// Bar chart option
// ============================================================== 

var myChart{{$rnd}} = echarts.init(document.getElementById('bar-chart{{$rnd}}'));

// specify chart configuration item and data
option{{$rnd}} = {
    tooltip : {
        trigger: 'axis'
    },
    grid: {
        left: 80,
        top: 20,
        right: 40,
        bottom: 80
    },
    legend: {
        data:["Semana actual","Semana anterior"],
        show: true,
        width: '100%',
        bottom: 0,
        left: '30%',
        height: 20,
        type: 'plain',
        orient: 'horizontal',
    },
    color: ["#F6BB9A","#9EC1DD"],
    calculable : true,
    xAxis : [
        {
            type : 'category',
            data : [@foreach($nomjuegos as $j) "{{ $j }}", @endforeach]
        }
    ],
    yAxis : [
        {
            type : 'value',
            axisLabel: {
                formatter: '{value}', 
            }
        }
    ],
    series : [
                {
                    name:'Semana actual',
                    type:'bar',
                    label: {
                        normal: {
                            show: true,
                            position: 'top',
                            fontWeight: 'bold',
                            fontSize: 16,
                            color: '#000'
                        }
                    },
                    data:[ 
                            @foreach($juegos as $j)
                                @php $datos_juego=$datos->where('COD_SORTEO',$j); @endphp
                                {{ round($datos_juego->first()->venta_total??0) }},
                            @endforeach
                         ],
                    
                },
                {
                    name:'Semana anterior',
                    type:'bar',
                    label: {
                        normal: {
                            show: true,
                            position: 'top',
                            fontWeight: 'bold',
                            fontSize: 16,
                            color: '#000'
                        }
                    },
                    data:[ 
                            @foreach($juegos as $j)
                                @php $datos_juego_ant=$datos_ant->where('COD_SORTEO',$j)->first()->venta_total??0; @endphp
                                {{ round($datos_juego_ant) }},
                            @endforeach
                         ]
                },
        ]
};

myChart{{$rnd}}.setOption(option{{$rnd}}, true), $(function() {
            function resize() {
                setTimeout(function() {
                    myChart{{$rnd}}.resize()
                }, 100)
            }
            //$(window).on("resize", resize), $(".sidebartoggler").on("click", resize)
        });

</script>