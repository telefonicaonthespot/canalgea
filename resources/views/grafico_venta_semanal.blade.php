{{-- 
  <script src="/vendor/moment/moment.js"></script>
  <script src="/vendor/echarts/dist/echarts.min.js"></script> --}}

@php
    $juegos=$datos->pluck('cod_juego')->unique();
    $nomjuegos=$datos->pluck('DES_SORTEO')->unique();
    $colores=$datos->pluck('val_color')->unique();
    $horas=$datos->pluck('hora_ins')->unique();
    $rnd=Illuminate\Support\Str::random(4);   

    header("Cache-Control: no-cache, no-store, must-revalidate"); // HTTP 1.1.
    header("Pragma: no-cache"); // HTTP 1.0.
    header("Expires: 0"); // Proxies.
@endphp

<div class="card">
    <div class="card-body">
    <div id="bar-chart{{$rnd}}" style="width:1360px; height:300px;"></div>
    </div>
</div>



<script>
// ============================================================== 
// Bar chart option
// ============================================================== 

var myChart{{$rnd}} = echarts.init(document.getElementById('bar-chart{{$rnd}}'));

// specify chart configuration item and data
option{{$rnd}} = {
    tooltip : {
        trigger: 'axis'
    },
    grid: {
        left: 80,
        top: 20,
        right: 40,
        bottom: 80
    },
    legend: {
        data:[@foreach($nomjuegos as $n) "{{ $n }}", @endforeach],
        show: true,
        width: '100%',
        bottom: 0,
        left: '30%',
        height: 20,
        type: 'plain',
        orient: 'horizontal',
    },
    toolbox: {
        show : false,
        feature : {
            
            magicType : {show: true, type: ['line', 'bar']},
            restore : {show: false},
            saveAsImage : {show: true}
        }
    },

    color: [@foreach($colores as $c) "{{ $c }}", @endforeach],
    calculable : true,
    xAxis : [
        {
            type : 'category',
            data : [@foreach($horas as $h) "{{ substr(str_pad($h, 4, '0', STR_PAD_LEFT),0,2) }}", @endforeach]
        }
    ],
    yAxis : [
        {
            type : 'value',
            axisLabel: {
                formatter: '{value}K', 
            }
        }
    ],
    series : [
            @foreach($juegos as $j)
            @php
                $datos_juego=$datos->where('cod_juego',$j)->sortby('hora_ins');
            @endphp
            {
                name:'{{$datos_juego->first()->DES_SORTEO}}',
                type:'line',
                showAllSymbol: true,
                //lineStyle.width=4,
                data:[ @foreach($datos_juego as $d)"{{round($d->venta_total/1000)}}",@endforeach],
                lineStyle: {width: 6}
            },
            @endforeach
        ]
};
// use configuration item and data specified to show chart
myChart{{$rnd}}.setOption(option{{$rnd}}, true), $(function() {
            function resize() {
                setTimeout(function() {
                    myChart{{$rnd}}.resize()
                }, 100)
            }
            //$(window).on("resize", resize), $(".sidebartoggler").on("click", resize)
        });

</script>