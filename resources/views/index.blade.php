@extends('layout')

@section('content')
    <div class="row">
        <div class="card col-12">
            @include('cabecera_terminales')
        </div>
    </div>
    <br>
    <div class="row">
        <div class="col-md-9">
            <div class="capa" id="paso1">

                <div class="row mt-4">
                    <div class="col-md-3">

                    </div>
                    <div class="col-md-1 font-weight-bold text-center mt-2">

                    </div>
                    <div class="col-md-2 font-weight-bold text-right mt-2 font-20">
                        BOTE
                    </div>
                    <div class="md-1"></div>
                    <div class="col-md-2 font-weight-bold text-right">
                        <span class="font-20">VENTA</span><br><span class="font-weight-light" style="font-size: 0.9em;"" >Juego + Asociado</span>
                    </div>
                    <div class="md-1"></div>
                    <div class="col-md-2  font-weight-bold text-right">
                        <span class="font-20">Internet+Peñas</span><br><span  class="font-weight-light" style="font-size: 0.9em">Juego + Asociado</span>
                    </div>
                </div>

                @foreach($sorteos_loto as $sorteo)
                    <div class="card mb-2 row_sorteo" id="sorteo_{{ $sorteo->id_sorteo }}">
                        <div @if($sorteos_loto->count()==8) class="row pt-1 pb-1" @else class="row pt-3 pb-3" @endif style="font-size: 29px; font-weight: bold; color: #999;">
                            <div class="col-md-3 {{ $sorteo->ESTILO_CSS }}">
                            {{-- {{  $sorteo->DES_SORTEO }} --}}
                            </div>
                            <div class="col-md-1 font-weight-bold text-center mt-2">
                                <span style="font-size: 1.4em">{{ Carbon\Carbon::parse($sorteo->FEC_SORTEO)->locale('es_ES')->format('d')  }}</span> <span style="font-size: 0.7em" class="float-right mt-3 ml-1">{{ Carbon\Carbon::parse($sorteo->FEC_SORTEO)->locale('es_ES')->Isoformat('MMM')  }}</span>
                            </div>
                            <div class="col-md-2 mt-3 text-right">
                                {{ number_format(round($sorteo->BOTE_SORTEO),0,",",".") }} €
                            </div>
                            <div class="md-1"></div>
                            <div class="col-md-2  mt-3  text-right">
                                {{ number_format(round($sorteo->VENTA_TOTAL_SORTEO),0,",",".")}} €
                            </div>
                            <div class="md-1"></div>
                            <div class="col-md-2 mt-3  text-right">
                                {{ number_format(round($sorteo->VENTA_INTERNET_SORTEO),0,",",".") }} €
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="capa " id="paso1b" style="display: none">

                <div class="row mt-4">
                    <div class="col-md-3">

                    </div>
                    <div class="col-md-1 font-weight-bold text-center mt-2">

                    </div>
                    <div class="col-md-2 font-weight-bold text-right mt-2 font-20">
                        BOTE
                    </div>
                    <div class="md-1"></div>
                    <div class="col-md-2 font-weight-bold text-right">
                        <span class="font-20">VENTA</span><br><span class="font-weight-light" style="font-size: 0.9em;"" >Juego + Asociado</span>
                    </div>
                    <div class="md-1"></div>
                    <div class="col-md-2  font-weight-bold text-right">
                        <span class="font-20">Internet+Peñas</span><br><span  class="font-weight-light" style="font-size: 0.9em">Juego + Asociado</span>
                    </div>
                </div>

                @foreach($sorteos_resto as $sorteo)
                    <div class="card mb-2 row_sorteo" id="sorteo_{{ $sorteo->id_sorteo }}">
                        <div @if($sorteos_resto->count()==8) class="row pt-1 pb-1" @else class="row pt-3 pb-3" @endif style="font-size: 29px; font-weight: bold; color: #999;">
                            <div class="col-md-3 {{ $sorteo->ESTILO_CSS }}">
                            {{-- {{  $sorteo->DES_SORTEO }} --}}
                            </div>
                            <div class="col-md-1 font-weight-bold text-center mt-2">
                                <span style="font-size: 1.4em">{{ Carbon\Carbon::parse($sorteo->FEC_SORTEO)->locale('es_ES')->format('d')  }}</span> <span style="font-size: 0.7em" class="float-right mt-3 ml-1">{{ Carbon\Carbon::parse($sorteo->FEC_SORTEO)->locale('es_ES')->Isoformat('MMM')  }}</span>
                            </div>
                            <div class="col-md-2 mt-3 text-right">
                                {{ number_format(round($sorteo->BOTE_SORTEO),0,",",".") }} €
                            </div>
                            <div class="md-1"></div>
                            <div class="col-md-2  mt-3  text-right">
                                {{ number_format(round($sorteo->VENTA_TOTAL_SORTEO),0,",",".")}} €
                            </div>
                            <div class="md-1"></div>
                            <div class="col-md-2 mt-3  text-right">
                                {{ number_format(round($sorteo->VENTA_INTERNET_SORTEO),0,",",".") }} €
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="capa" id="paso2"  style="display: none">
                <div class="row mb-4">
                    <div class="col-md-3">
                        <img src="{{url('/img/nacional.png')}}" style="height: 70px">
                    </div>
                    <div class="col-md-3"></div>
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-body" style="font-size: 22px">
                                Pagos Lotería Nacional (semana en curso): <span class="font-weight-bold float-right" >@isset($datos_genericos){{ number_format($datos_genericos->PSLN,0,",",".") }} €@endisset</span>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- <div class="row mt-2">
                    <div class="col-md-6">
                        <div class="card border-left-primary shadow h-70 py-2  bg-light-primary">
                            <div class="card-body" style="padding-top: 2px; padding-bottom: 2px">
                            <div class="row no-gutters align-items-center">
                                <div class="col mr-2">
                                <div class="text-xs font-weight-bold text-primary text-uppercase mb-1 font-20">Venta preimpresa <span class="text-muted ml-4 font-bold" style="font-size:26px">{{number_format(valor_total($lot_nacional->take(5),"VALOR_PREIMPRESOS_PROVS","num"),0,",",".")}}€</span></div>
                                {{-- <div class="h5 mb-0 font-weight-bold text-gray-800">40,000</div> --}}
                                </div>
                                <div class="col-auto">
                                <i class="fas fa-ticket-alt fa-2x text-gray-300"></i>
                                </div>
                            </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-1">
                       &nbsp;
                    </div>
                    <div class="col-md-5">
                        <div class="card border-left-success shadow h-70 py-2  bg-light-success">
                            <div class="card-body" style="padding-top: 2px; padding-bottom: 2px">
                            <div class="row no-gutters align-items-center">
                                <div class="col mr-2">
                                <div class="text-xs font-weight-bold text-success text-uppercase mb-1 font-20">Venta en línea <span class="text-muted  ml-4 font-bold" style="font-size:26px">{{number_format(valor_total($lot_nacional->take(5),"VALOR_VENTA_TOTAL","num"),0,",",".")}}€</span></div>
                                </div>
                                <div class="col-auto">
                                <i class="fas fa-desktop fa-2x text-gray-300"></i>
                                </div>
                            </div>
                            </div>
                        </div>
                    </div>
                </div> -->

                @foreach($lot_nacional->take(3) as $ln)
                @php
                    //dd($ln);
                @endphp
                <div class="card mt-4">
                    <div class="card-body">
                        <div class="row font-18">
                            <div class="col-md-8 bg-gray-400  text-uppercase text-primary">
                                SORTEO DEL {{dayOfWeek2(Carbon\Carbon::parse($ln->FEC_SORTEO_LN)->dayOfWeek,2)}} {{ Carbon\Carbon::parse($ln->FEC_SORTEO_LN)->locale('es_ES')->isoFormat('LL') }}
                            </div>
                            <div class="col-md-2 bg-gray-400">
                                Emisión: {{ number_format($ln->BILLETES_SORTEO,0,",",".") }}
                            </div>
                            <div class="col-md-2 bg-gray-400">
                                Precio: {{ $ln->PRECIO_BILLETE }}€
                            </div>

                        </div>
                        <div class="row" style="font-size: 18px">
                            <div class="col-md-2 text-center mt-0 bg-light-primary">
                                Consignada preimpresa
                            </div>
                            <div class="col-md-2  text-center mt-0 bg-light-primary">
                                Transmitida preimpresa
                            </div>
                            <div class="col-md-2  text-center mt-0 bg-light-primary">
                                Comunicada preimpresa
                            </div>
                            
                            <div class="col-md-2  text-center mt-0 bg-light-primary">
                                Preventa preimpresa
                            </div>
                            <div class="col-md-2  text-center mt-0 bg-light-primary">
                               Venta terminal
                            </div>
                            
                        </div>
                        <div class="row" style="font-size: 30px">
                            <div class="col-md-2 font-weight-bold text-center mt-0">
                                {{ number_format($ln->VALOR_PREIMPRESOS_CONSG,0,",",".") }}€
                            </div>
                            <div class="col-md-2 font-weight-bold text-center mt-0">
                                {{ number_format($ln->VALOR_TRANSMITIDA_PREIMPRESA,0,",",".") }}€
                            </div>
                            <div class="col-md-2 font-weight-bold text-center mt-0">
                                {{ number_format($ln->VALOR_COMUNICADA_PREIMPRESA,0,",",".") }}€
                            </div>
                            <div class="col-md-2 font-weight-bold text-center mt-0">
                                {{ number_format($ln->VALOR_PREVENTA_PREIMPRESA,0,",",".") }}€
                            </div>
                            <div class="col-md-2 font-weight-bold text-center mt-0">
                                {{ number_format($ln->VALOR_VENTA_TOTAL,0,",",".") }}€
                            </div>
                            
                        </div>
                    </div>
                </div>
                @endforeach
                @if(isset($lot_navidad))
                <div class="card mt-4 bg-gray-400">
                    <div class="card-body">
                        <div class="row font-18">
                            <div class="col-md-8 bg-gray-400  text-uppercase text-primary">
                                SORTEO DEL {{dayOfWeek2(Carbon\Carbon::parse($lot_navidad->FEC_SORTEO_LN)->dayOfWeek,2)}} {{ Carbon\Carbon::parse($lot_navidad->FEC_SORTEO_LN)->locale('es_ES')->isoFormat('LL') }}
                            </div>
                            <div class="col-md-2 bg-gray-400">
                                Emisión: {{ number_format($lot_navidad->BILLETES_SORTEO,0,",",".") }}
                            </div>
                            <div class="col-md-2 bg-gray-400">
                                Precio: {{ $lot_navidad->PRECIO_BILLETE }}€
                            </div>

                        </div>
                        <div class="row"  style="font-size: 18px">
                            <div class="col-md-2 text-center mt-0 bg-light-primary">
                                Consignada preimpresa
                            </div>
                            <div class="col-md-2  text-center mt-0 bg-light-primary">
                                Transmitida preimpresa
                            </div>
                            <div class="col-md-2  text-center mt-0 bg-light-primary">
                                Comunicada preimpresa
                            </div>
                            
                            <div class="col-md-2  text-center mt-0 bg-light-primary">
                                Preventa preimpresa
                            </div>
                            <div class="col-md-2  text-center mt-0 bg-light-primary">
                               Venta terminal
                            </div>
                            <div class="col-md-2">
                                &nbsp;
                            </div>
                        </div>
                        <div class="row" style="font-size: 30px">
                            <div class="col-md-2 font-weight-bold text-center mt-0">
                                {{ number_format($lot_navidad->VALOR_PREIMPRESOS_CONSG,0,",",".") }}€
                            </div>
                            <div class="col-md-2 font-weight-bold text-center mt-0">
                                {{ number_format($lot_navidad->VALOR_TRANSMITIDA_PREIMPRESA,0,",",".") }}€
                            </div>
                            <div class="col-md-2 font-weight-bold text-center mt-0">
                                {{ number_format($lot_navidad->VALOR_COMUNICADA_PREIMPRESA,0,",",".") }}€
                            </div>
                            <div class="col-md-2 font-weight-bold text-center mt-0">
                                {{ number_format($lot_navidad->VALOR_PREVENTA_PREIMPRESA,0,",",".") }}€
                            </div>
                            <div class="col-md-2 font-weight-bold text-center mt-0">
                                {{ number_format($lot_navidad->VALOR_VENTA_TOTAL,0,",",".") }}€
                            </div>
                            <div class="col-md-2">
                                &nbsp;
                            </div>
                        </div>
                    </div>
                </div>
                @endif
            </div>
            <div class="capa" id="paso3"  style="display: none">
                <h4>Venta semanal acumulada por juego</h4>
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body" id="venta_semanal1">

                            </div>
                        </div>

                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body" id="venta_semanal2">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="capa" id="paso4"  style="display: none">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body" id="ventas_hora">

                            </div>
                        </div>

                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 mt-3">
                        <div class="card">
                            <div class="card-body" id="trans_hora">

                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 mt-3">
                        <div class="card">
                            <div class="card-body" id="term_hora">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="capa" id="paso5"  style="display: none">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body" id="ventas_anio">

                            </div>
                        </div>

                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 mt-3">
                        <div class="card">
                            <div class="card-body" id="trans_anio">

                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 mt-3">
                        <div class="card">
                            <div class="card-body" id="term_anio">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="capa" id="paso6"  style="display: none">
                <h4 class="text-center">Ventas acumuladas por juego [ {{Carbon\Carbon::now()->format('d/m/Y')}}]</h4>
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body" id="ventas_acumulada1">

                            </div>
                        </div>

                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body" id="ventas_acumulada2">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        {{-- Graficos de la derecha --}}
        <div class="card col-md-3">
            <div class="row">
                <div class="col-12">
                    {{-- @php dd($datos_financieros); @endphp; --}}
                    @include('grafico_circulo_sorteos',$graficos_quesito)
                    <br>
                    @include('grafico_barras_financieros',$datos_financieros)
                </div>
            </div>
            <div class="row">
                <div class="col-md-12 text-center mt-2 pt-3">
                    <b class="font-18">MMXX</b> &nbsp;&nbsp;<img src="{{url('img/loterias_logo26.png')}}">
                </div>

            </div>

        </div>
    </div>
@endsection


@section('scripts')
<script>
function sleep(milliseconds) {
  var start = new Date().getTime();
  for (var i = 0; i < 1e7; i++) {
    if ((new Date().getTime() - start) > milliseconds){
      break;
    }
  }
}

function update_cabecera(){
    console.log('update cabecera');
    $.get('{{ url("cabecera_terminales") }}?' + new Date().getTime(),function(data){
        console.log('ok cabecera');
        $(".cabecera-terminales").html(data);
        load_pastillas(0);
     });
}

// $.each( $('.row_sorteo'), function() {
//     console.log($(this).attr('id'));
//     animateCSS('#'+$(this).attr('id'),'bounceInLeft');
//          sleep(150);

//     });

let url=["{{ url('venta_semanal/'.'J1,J2,J3,J4,J14') }}","{{ url('venta_semanal/'.'J5,J6,J7,J8') }}","{{ url('porhora/ventas') }}","{{ url('porhora/transacciones') }}","{{ url('porhora/terminales') }}","{{ url('vsanio/ventas') }}","{{ url('vsanio/transacciones') }}","{{ url('vsanio/terminales') }}","{{ url('acumuladas/J1,J2,J3,J4,J14') }}","{{ url('acumuladas/J5,J6,J7,J8') }}"];
let zonas=['#venta_semanal1','#venta_semanal2','#ventas_hora','#trans_hora','#term_hora','#ventas_anio','#trans_anio','#term_anio','#ventas_acumulada1','#ventas_acumulada2'];

function load_pastillas(index){
    console.log('load '+url[index]+' en '+zonas[index]);
    $.get(url[index]+'?_=' + new Date().getTime(),function(data){
        console.log('ok');
        $(zonas[index]).html(data);
         //animateCSS('#venta_semanal1','bounceInRight');
     });
}


$(function(){
    //
    let index=0;
    let playlist=["paso1","paso1b","paso2","paso3","paso1","paso1b","paso4","paso5","paso6"];
    let nowplay=1;
    let intervalo=12000;
    let debug="";

    function ocultar(){
        $('#paso1').hide(); //Sorteos loto
        $('#paso1b').hide();  //Sorteos resto
        $('#paso2').hide();  //Loteira nacional
        $('#paso3').hide(); //venta semanal
        $('#paso4').hide(); //por hora
        $('#paso5').hide(); //por año
        $('#paso6').hide(); //acumuladas
    }

    carga=setInterval(function(){
        if (index==url.length){
            clearInterval(carga);
        } else{
            load_pastillas(index);
            index++;
        }
    }, 500);

    proceso=setInterval(function(){
        ocultar();
        console.log('Reproducir '+'#'+playlist[nowplay]);
        $('#'+playlist[nowplay]).show();
        $('.grafico').children().width('100%');
        animateCSS('#'+playlist[nowplay],'bounceInRight');
        nowplay++;
        if (nowplay==playlist.length){
            nowplay=1;
        }
    },intervalo);

    timer_cabecera=setInterval(function(){
        update_cabecera();
    }, 30000);

    if(debug!=0){
        clearInterval(proceso);
        console.log('debug '+debug);
        ocultar();
        $('#paso'+debug).show();
        
    }
})
</script>
@endsection
