<!DOCTYPE html>
<html lang="en">

<head>
  <base href="/canalgea3/">
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>SB Admin 2 - Dashboard</title>
  <link href="{{ url('vendor/bootstrap/bootstrap.min.css') }}" rel="stylesheet">
  <!-- Custom fonts for this template-->
  <link href="{{ url('vendor/fontawesome-free/css/all.min.css') }}" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
  <!-- Custom styles for this template-->
  <link href="{{ url('css/canalgea.css') }}" rel="stylesheet">
  <link href="{{ url('css/sb-admin-2.css') }}" rel="stylesheet">
  {{-- <link href="css/style.css" rel="stylesheet"> --}}
  {{-- <link href="css/style-dark.css" rel="stylesheet">
  <link href="css/colors/blue.css" rel="stylesheet"> --}}
  <link href="{{ url('vendor/animate.css/dist/animate.min.css') }}" rel="stylesheet">
  <link href="{{ url('vendor/chartist-js/dist/chartist.min.css') }}" rel="stylesheet">
  <link href="{{ url('vendor/css-chart/css-chart.css') }}" rel="stylesheet">
  <link href="{{ url('vendor/csshake/dist/csshake.min.css') }}" rel="stylesheet">

  <style type="text/css">
    html, body {
      overflow-y:hidden;
      height:100%;
    }
  </style>
  @php
    header("Cache-Control: no-cache, no-store, must-revalidate"); // HTTP 1.1.
    header("Pragma: no-cache"); // HTTP 1.0.
    header("Expires: 0"); // Proxies.
  @endphp

  @yield('estilos')
</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">


    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">
          <!-- Topbar Navbar -->
          <ul class="navbar-nav ml-auto">
            <li class="float-left">
                <img src="{{url('img/ICONO.png')}}" style="height: 70px;">
            </li>
            
            <li class="ml-3 mt-2 text-center" style="width: 1650px">
               <h1 class="font-weight-bold">INFORMACION DE SORTEOS EN CURSO</h1>
            </li>
            <li class="text-center mt-2">
                <h5>{{ Carbon\Carbon::now()->locale('es_ES')->setTimezone('Europe/Madrid')->isoFormat('DD MMMM  YYYY') }}</h4>
                <h6 class="float-right">{{ Carbon\Carbon::now()->setTimezone('Europe/Madrid')->format('H:i') }}</h6>
            </li>
          </ul>

        </nav>
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">
            @yield('content')
        </div>
        <!-- /.container-fluid -->
      </div>
      <!-- End of Main Content -->
    </div>
    <!-- End of Content Wrapper -->
  </div>
  <!-- End of Page Wrapper -->

  <!-- Bootstrap core JavaScript-->
  <script src="{{ url('vendor/jquery/jquery.min.js') }}"></script>
  <script src="{{ url('vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>

  <!-- Core plugin JavaScript-->
  <script src="{{ url('vendor/jquery-easing/jquery.easing.min.js') }}"></script>

  <!-- Custom scripts for all pages-->
  <script src="{{ url('js/sb-admin-2.js') }}"></script>

  <!-- Page level plugins -->
  <script src="{{ url('vendor/chart.js/Chart.min.js') }}"></script>

  <!-- Page level custom scripts -->
  {{-- <script src="js/demo/chart-area-demo.js"></script>
  <script src="js/demo/chart-pie-demo.js"></script> --}}

  <script src="{{ url('vendor/chartist-js/dist/chartist.min.js') }}"></script>
  <script src="{{ url('vendor/moment/moment.js') }}"></script>
  <script src="{{ url('vendor/echarts/dist/echarts.min.js') }}"></script>
  <script>
  function animateCSS(element, animationName, callback) {
            const node = document.querySelector(element)
            node.classList.add('animated', animationName)

            function handleAnimationEnd() {
                node.classList.remove('animated', animationName)
                node.removeEventListener('animationend', handleAnimationEnd)

                if (typeof callback === 'function') callback()
            }

            node.addEventListener('animationend', handleAnimationEnd)
        }
  </script>
  @yield('scripts')
  @yield('scripts2')
  @yield('scripts3')
  @yield('scripts4')
  @yield('scripts5')
  @yield('scripts6')

</body>

</html>
