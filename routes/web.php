<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/','IndexController@index');
Route::get('venta_semanal/{juegos}','IndexController@venta_semanal');
Route::get('porhora/{tipo}','IndexController@por_hora');
Route::get('vsanio/{tipo}','IndexController@vsanio');
Route::get('acumuladas/{tipo}','IndexController@ventas_acumuladas');
Route::get('/cabecera_terminales','IndexController@cabecera_terminales');

route::view('layout','layout');


